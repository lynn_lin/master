"""
CA1 ECG analysis

Student name:Xiaolin Yu 
"""
#2.2 Acquiring and loading data
import sys
import numpy as np
# imports go here (anything in numpy/scipy is fine)

# re-usable functions / classes go here

def analyse_ecg(signal_filename, annotations_filename):
    # basic formatted output in Python
    print('signal filename: %s' % signal_filename)
    print('annotation filename: %s' % annotations_filename)
    
    # your analysis code here    
    data = np.loadtxt(signal_filename, delimiter=",",skiprows=2)
    print(data)
    dataTimes = tuple(x[0] for x in data)
    timeLength = dataTimes[-1] - dataTimes[0]
    samplingRate = len(data) / timeLength
    print("Sampling Rate: %3.2f" % samplingRate + "Hz")
if __name__=='__main__':
    analyse_ecg("input1" + "_signals.txt", "input1"+"_annotations.txt")

import matplotlib.pyplot as plt

x=np.arange(0,10,0.01)
y=np.sin(2*np.pi*x)
  
plt.figure(figsize=(10,5))
plt.grid()

plt.plot(x,y, label="$sin(2*np.pi*x)$", color = "red",Linewidth=1.5)
plt.xlabel("Time(s)")
plt.ylabel("Volt")
plt.title("PyPlot Visualisation")
plt.ylim(-1.5,1.5)
plt.legend()
plt.show()


import matplotlib.pyplot as plt
import numpy as np


def f(t):
    s1 = np.sin(2*np.pi*t)
    e1 = np.exp(-t)
    return np.multiply(s1, e1)

t1 = np.arange(0, 10, 0.1)
t2 = np.arange(0, 10,0.02)

fig, ax = plt.subplots()
plt.plot(t1, f(t1), 'bo', t2, f(t2), 'k')
plt.text(3.0, 4, 'f(t) = exp(-t) sin(2*np.pi*t)')
ttext = plt.title('Get ECG Value')
ytext = plt.ylabel('Volt')
xtext = plt.xlabel('time (s)')

plt.setp(ttext, size='large', color='r', style='italic')
plt.setp(xtext, size='medium', name=['Courier', 'Bitstream Vera Sans Mono'],
     weight='bold', color='g')
plt.setp(ytext, size='medium', name=['Helvetica', 'Bitstream Vera Sans'],
     weight='light', color='b')
plt.show()




 
