CA1
===

Modify this file to complete the following questions.
Other than filling in required information, please *DO NOT* make any changes to this file's structure or the question text (to ease grading).

Student information
-------------------

Name (as on Moodle): Peadar Grant

Sortable name (no spaces, surname then firstname): grant_peadar

Reflection questions (after coding)
-----------------------------------

When you have finished coding, please reflect briefly on the following three questions. Marks here are awarded for engaging with the question - they're not a trick, and there is no "right" answer.

Question 1
^^^^^^^^^^

If you had much more time to work on this problem, how would you attempt to improve your code? (Suggested length: one short paragraph)

YOUR ANSWERE HERE.
I shall focus on sever programming textbook of python to study the basic first, and then look for online forum of python, there is much open code to learn.

Question 2
^^^^^^^^^^

What is the most important thing that you learned from this lab exercise? (Suggested length: one sentence)

YOUR ANSWERE HERE.
to review the theoratical and formula of signal study, and transfer them into practical graphic.

Question 3
^^^^^^^^^^

What did you like/dislike the most about this lab exercise? (Suggested length: one sentence)

YOUR ANSWER HERE.

I like it and feel happy each time get any improvement and understanding in this course.
